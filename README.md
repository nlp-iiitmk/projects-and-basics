# projects and basics

## * Projets

### **Project List** file contains suggestions for the projects to be done in the NLP group.

### Kindly provide things to work on in **Project List** file and raise an issue about it.
 
 
 
 
 
 

## * Basics To Natural Language Processing Group

This repo will contain all the relevant information for the usage of the projects.
### How projects are stored
At any point of time there will be two copies of the project. 
* **master** Branch   : This copy will be visible to everyone and will be the official copy. Only be editable by the _Owner_. 
* **developer** / **working** Branch: This will be used by everyone to work on the project and make changes. Only editable by _Maintainer_ 

### How to use this repo?

  

1. Fork any project to your profile

1. Clone the forked repo and make any changes in your machine

1. Push the changes onto your fork

#### For Developers
* Submit a merge request from the forker repo on your profile to the **developer** branch 
#### For Maintainers
* Accept the merge requests from _Developers_ to the **developer** branch if the changes are authentic and proper
* Submit a merge request from the **developer** branch to the **master** branch
#### For Owners
* Accept the merge requests from _Maintainers_ to the **master* branch if the changes are authentic and proper

### Learn how to use git by checking out the **git for dummies** project
### To Try _Basic NLP Examples_, To clarify any Doubts regarding _NLP_ or _Git Projects_ Email me at sukesh.mi3@iitmk.ac.in  
 